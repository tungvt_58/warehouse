package controllers;

import models.StockItem;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by admin on 20/03/2015.
 */
public class StockItems extends Controller {
    public static Result index() {
        List<StockItem> items = StockItem.find.findList();
        return ok(items.toString());
    }
}
